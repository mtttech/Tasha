from dataclasses import dataclass, field
from math import ceil
from typing import Any


@dataclass
class Character:
    alignment: str = field(default="")
    allotted_asi: int = field(default=0)
    allotted_skills: int = field(default=0)
    armors: list = field(default_factory=list)
    attributes: dict = field(default_factory=dict)
    background: str = field(default="Soldier")
    bonus: dict = field(default_factory=dict)
    cantrips: list = field(default_factory=list)
    classes: dict = field(default_factory=dict)
    equipment: list = field(default_factory=list)
    feats: list = field(default_factory=list)
    features: list = field(default_factory=list)
    gender: str = field(default="")
    gold: int = field(default=0)
    height: int = field(default=0)
    hit_points: int = field(init=False)
    initiative: int = field(default=0)
    known_spells: dict = field(default_factory=dict)
    languages: list = field(default_factory=list)
    level: int = field(default=1)
    name: str = field(default="Unnamed")
    prepared_spells: dict = field(default_factory=dict)
    proficiency_bonus: int = field(default=0)
    race: str = field(default="")
    resistances: list = field(default_factory=list)
    savingthrows: list = field(default_factory=list)
    size: str = field(default="Medium")
    skills: list = field(default_factory=list)
    speed: int = field(default=30)
    spell_slots: list = field(default_factory=list)
    tools: list = field(default_factory=list)
    traits: list = field(default_factory=list)
    version: str = field(default="")
    weapons: list = field(default_factory=list)
    weight: int = field(default=0)

    def __post_init__(self):
        self.hit_points = self.roll_hit_points()
        self.proficiency_bonus = ceil(self.level / 4) + 1
        try:
            self.initiative = self.attributes["Dexterity"]["modifier"]
        except KeyError:
            self.initiative = 0

    def roll_hit_points(self) -> int:
        """Calculates the character's total hit points."""
        try:
            modifier = self.attributes["Constitution"]["modifier"]
        except KeyError:
            modifier = 0

        total_hit_points = 0
        for class_slot, klass in enumerate(tuple(self.classes.keys())):
            max_hit_die = self.classes[klass]["hit_die"]
            avg_hit_die = ceil(max_hit_die / 2) + 1

            if class_slot == 0:
                total_hit_points = max_hit_die + modifier
            else:
                total_hit_points += avg_hit_die + modifier

            if self.classes[klass]["level"] > 1:
                total_hit_points += sum(
                    [
                        avg_hit_die + modifier
                        for _ in range(1, self.classes[klass]["level"])
                    ]
                )

        try:
            return total_hit_points
        except UnboundLocalError:
            return 0

    def __setitem__(self, name: str, value: Any) -> None:
        key_value = eval(f"self.{name}")
        # Append dictionary value to existing dictionary value.
        if isinstance(key_value, dict) and isinstance(value, dict):
            exec(f"self.{name}.update({value})")
        # Append new attribute, assign value of 1. TODO: Investigate usage further
        elif isinstance(key_value, dict) and isinstance(value, str):
            exec(f"self.{name} = 1")
        # Append list value to an existing list value.
        elif isinstance(key_value, list) and isinstance(value, list):
            exec(f"self.{name} = self.{name} + {value}")
        # Append str value to an existing list value.
        elif isinstance(key_value, list) and isinstance(value, str):
            exec(f'self.{name}.append("{value}")')
        else:
            if isinstance(value, str):
                exec(f'self.{name} = "{value}"')
            else:
                exec(f"self.{name} = {value}")

    def set(self, *args) -> None:
        num_of_args = len(args)
        if num_of_args == 1 and isinstance(args[0], dict):
            for key, value in args[0].items():
                self.__setitem__(key, value)
        elif num_of_args == 2:
            key, value = args
            self.__setitem__(key, value)
        else:
            raise ValueError(f"Accepts 1-2 arguments. {num_of_args} given.")
